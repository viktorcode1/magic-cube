# Magic Cube

Magic Cube Puzzle Game

## Project structure

All code is separated into 4 categories: UI scripts, Gameplay scripts, Cube model specific scripts, and generic purpose helper code.

`ScenesNavigation` is the main class managing app flow. It's responsible for switching between app screens in response to player's actions.

`GameplayController` is another persistent class, responsible for handling gameplay-related events and for storing game state. 

The rest of the types are focused on a single area each, and are pretty simple and self describing. There's no inheritance among custom classes.

There's no Magic Cube data model in the game. Instead, the game scene serves as source of truth.

## Magic Cube mechanics

Each cube model consists of a set of elements, called "cubies". A set of cubies form a slice, that can be rotated. The pivots are the fixed elements inside cube model, placed along three central axes.

Rotation happens in 3 steps: first, the rotation pivot is set as the parent to all cubies in its rotation plane. Then the pivot is rotated, which in turn rotates its children. After rotation is completed the cubies are deparented from the pivot.

Each cubie has 3, 2 or 1 colored face (for corner, edge, and side positions). At the start of the game faces on each side of the cube are assigned a color. There's no in-game code model to track the state of the cube, so after each rotation the faces of every side is checked for winning condition. 

Faces are the only colliders on the scene. Player drags them to rotate the cube slices. Dragging outside of a face rotates the game camera.

For persisting the game state between sessions the colors of the cube's faces are collected and stored. The game is saved into an xml file. Timer visibility status is kept in PlayerPrefs. 

## Testing

In order to get an easy win, uncomment the marked line in `GameplayController.ScrambleCube()` method. That will enable undo stack for scrambling phase, so Undo button presses will get you to winning condition. You can decrease the number of scrambling iterations too.
