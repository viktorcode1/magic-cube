using UnityEngine;

namespace Game
{
    public sealed class RotationPivot : MonoBehaviour
    {
        public PlaneSide SelectionPlane;
        public Plane RotationPlane { get; private set; }

        void Awake()
        {
            var axis = SelectionPlane.RotationNormalAt(transform.localPosition);
            RotationPlane = new Plane(axis, transform.position);
        }
    }

    public enum PlaneSide
    {
        XY, XZ, YZ
    }

    public static class PlaneSideExtension
    {
        public static Vector3 RotationNormalAt(this PlaneSide planeSide, Vector3 position)
        {
            return planeSide switch {
                PlaneSide.XY => position.z > 0 ? Vector3.forward : Vector3.back,
                PlaneSide.XZ => position.y > 0 ? Vector3.up : Vector3.down,
                _ => position.x > 0 ? Vector3.right : Vector3.left
            };
        }
    }
}