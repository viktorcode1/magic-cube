using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Colored element of the cube
    /// </summary>
    public sealed class Face : MonoBehaviour
    {
        static readonly Dictionary<FaceColor, Material> materialsCache = new Dictionary<FaceColor, Material>();
        public Cubie Owner;
        
        Vector3 planeNormal;
        public Vector3 Normal => transform.TransformVector(planeNormal).normalized;
        public FaceColor Color {
            get => color;
            set {
                if (!materialsCache.TryGetValue(value, out var material)) {
                    material = Resources.Load<Material>(value.MaterialAssetPath());
                    materialsCache[value] = material;
                }
                GetComponent<MeshRenderer>().material = material;
                color = value;
            }
        }
        FaceColor color;
        
        void Awake()
        {
            var quadVertices = GetComponent<MeshFilter>().mesh.vertices;
            planeNormal = new Plane(quadVertices[0], quadVertices[1], quadVertices[2]).normal;
        }
    }

    public enum FaceColor
    {
        White, Green, Orange, Yellow, Blue, Red
    }

    public static class FaceColorExtension
    {
        public static string MaterialAssetPath(this FaceColor color)
        {
            return color switch {
                FaceColor.White => "Materials/White Face",
                FaceColor.Green => "Materials/Green Face",
                FaceColor.Orange => "Materials/Orange Face",
                FaceColor.Yellow => "Materials/Yellow Face",
                FaceColor.Blue => "Materials/Blue Face",
                _ => "Materials/Red Face"
            };
        }
    }
    
    /// <summary>
    /// Allows sorting Faces on a cube in a deterministic way
    /// </summary>
    public sealed class FaceSortingComparer : Comparer<Face>
    {
        public override int Compare(Face f1, Face f2)
        {
            if (ReferenceEquals(f1, f2)) return 0;
            
            var v1 = f1.transform.position;
            var v2 = f2.transform.position;
            
            var diff = CompareApproximately(v1.x, v2.x);
            if (diff != 0) return diff;
            diff = CompareApproximately(v1.y, v2.y);
            if (diff != 0) return diff;
            diff = CompareApproximately(v1.z, v2.z);
            return diff;
            
            // Very coarse comparison which is good for our case of the cube with known transform in the world
            int CompareApproximately(float a, float b) => Mathf.Abs(a - b) < 0.01 ? 0 : (a > b ? 1 : -1);
        }
    }
}