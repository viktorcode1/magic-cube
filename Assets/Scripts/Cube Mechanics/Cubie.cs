using UnityEngine;

namespace Game
{
    /// <summary>
    /// Element that makes up the cube
    /// </summary>
    public sealed class Cubie : MonoBehaviour
    {
        public MeshRenderer meshRenderer { get; private set; }

        void Awake() { meshRenderer = GetComponent<MeshRenderer>(); }
    }
}