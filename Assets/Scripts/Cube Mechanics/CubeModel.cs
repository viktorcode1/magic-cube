using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public sealed class CubeModel : MonoBehaviour
    {
        Cubie[] Cubies;
        public RotationPivot[] Pivots;
        Face[] Faces;
        
        public int PivotsCount => Pivots.Length;
        public bool IsRotating { get; private set; }

        void Awake()
        {
            Cubies = GetComponentsInChildren<Cubie>();
            Faces = GetComponentsInChildren<Face>();
        }
        
        void OnDisable() { StopAllCoroutines(); }

        public RotationPivot GetPivot(int index) => Pivots[index];

        public int GetIndex(RotationPivot pivot) => Array.IndexOf(Pivots, pivot);

        public void InitializeDefaultColors()
        {
            foreach (var face in Faces) {
                var orientation = face.Normal.GetOrientation();
                face.Color = orientation switch {
                    Orientation.Right => FaceColor.White,
                    Orientation.Up => FaceColor.Green,
                    Orientation.Forward => FaceColor.Orange,
                    Orientation.Left => FaceColor.Yellow,
                    Orientation.Down => FaceColor.Blue,
                    Orientation.Back => FaceColor.Red,
                    _ => face.Color
                };
            }
        }

        /// <summary>
        /// Ordered collection of colors of cube's faces
        /// </summary>
        public FaceColor[] EnumerateColors()
        {
            Array.Sort(Faces, new FaceSortingComparer());
            var array = Faces.Select(f => f.Color).ToArray();
            return array;
        }

        /// <summary>
        /// Applies sorted collection of colors to the cube
        /// </summary>
        public void ApplyEnumeratedColors(FaceColor[] colors)
        {
            Array.Sort(Faces, new FaceSortingComparer());
            for (var i = 0; i < Faces.Length; i++) {
                Faces[i].Color = colors[i];
            }
        }

        /// <summary>
        /// Checks for completed cube
        /// </summary>
        /// <returns>True, if each cube side has faces only of a single color </returns>
        public bool CheckForWin()
        {
            var foundColors = new Dictionary<Orientation, FaceColor>();
            foreach (var face in Faces) {
                var orientation = face.Normal.GetOrientation();
                if (foundColors.TryGetValue(orientation, out var color)) {
                    if (color != face.Color)
                        return false;
                } else foundColors[orientation] = face.Color;
            }
            
            return true;
        }
        
        public IEnumerator RotateSlice(RotationPivot pivot, bool positiveAngle = true, float duration = 0.5f)
        {
            IsRotating = true;
            
            var cubies = FindCubiesForPivot(pivot);
            cubies.SetParentForAll(pivot.transform);
            
            float timeElapsed = 0;
            var pivotTransform = pivot.transform;
            var axis = pivot.RotationPlane.normal;
            var startRotation = pivotTransform.rotation;
            var targetRotation = startRotation * Quaternion.Euler(axis * (positiveAngle ? 90 : -90));
            while (timeElapsed < duration) {
                yield return null;
                pivotTransform.rotation = Quaternion.Slerp(startRotation, targetRotation, timeElapsed / duration);
                timeElapsed += Time.deltaTime;
            }
            pivotTransform.rotation = targetRotation;
            yield return null;
            
            cubies.SetParentForAll(transform);

            IsRotating = false;
        }

        List<Transform> FindCubiesForPivot(RotationPivot pivot)
        {
            var result = new List<Transform>();
            foreach (var cubie in Cubies) {
                var bounds = cubie.meshRenderer.bounds;
                if (bounds.IntersectsPlane(pivot.RotationPlane)) {
                    result.Add(cubie.transform);
                }
            }
            return result;
        }

        public List<RotationPivot> FindPivotsForCubie(Cubie cubie)
        {
            var result = new List<RotationPivot>();
            foreach (var pivot in Pivots) {
                var bounds = cubie.meshRenderer.bounds;
                if (bounds.IntersectsPlane(pivot.RotationPlane)) {
                    result.Add(pivot);
                }
            }
            return result;
        }
    }

    public enum Orientation
    {
        Right, Left, Up, Down, Forward, Back, Unknown
    }

    public static class OrientationExtension
    {
        public static Orientation GetOrientation(this Vector3 vec)
        {
            if (vec == Vector3.right) return Orientation.Right;
            if (vec == Vector3.left) return Orientation.Left;
            if (vec == Vector3.up) return Orientation.Up;
            if (vec == Vector3.down) return Orientation.Down;
            if (vec == Vector3.forward) return Orientation.Forward;
            if (vec == Vector3.back) return Orientation.Back;
            return Orientation.Unknown;
        }
    }
}
