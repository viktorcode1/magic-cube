using System.Collections;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Class for controlling camera movement in orbit around central point
    /// </summary>
    public sealed class CameraOrbiter : MonoBehaviour
    {
        public Camera Cam { get; private set; }
        float distance = 14f;
        const float verticalSensitivity = 200f;
        const float horizontalSensitivity = 200f;
        const float closestCameraDistance = 10f;
        const float farthestCameraDistance = 25f;

        void Awake() { Cam = GetComponent<Camera>(); }

        void Start() { SetDefaultPosition(); }

        /// <summary>
        /// Sets the default camera angle and distance
        /// </summary>
        void SetDefaultPosition()
        {
            Cam.transform.position = new Vector3(distance, distance, -distance);
            Cam.transform.LookAt(Vector3.zero);
            Cam.transform.position = Vector3.zero;
            Cam.transform.Translate(new Vector3(0, 0, -distance));
        }

        public void AdjustDistance(float movement)
        {
            distance = Mathf.Clamp(movement + distance, closestCameraDistance, farthestCameraDistance);
            Cam.transform.LookAt(Vector3.zero);
            Cam.transform.position = Vector3.zero;
            Cam.transform.Translate(new Vector3(0, 0, -distance));
        }
        
        /// <summary>
        /// Rotates the camera along the swipe direction
        /// </summary>
        public void Rotate(Vector3 swipeDirection)
        {
            Cam.transform.position = Vector3.zero; // Move to center so we can rotate correctly
            Cam.transform.Rotate(Vector3.right, swipeDirection.y * verticalSensitivity);
            Cam.transform.Rotate(Vector3.down, swipeDirection.x * horizontalSensitivity, Space.World);
            Cam.transform.Translate(Vector3.back * distance); // Now move rotated camera back the same distance
        }

        /// <summary>
        /// Performs orbiting flight around the central point 
        /// </summary>
        public IEnumerator FlyAround()
        {
            var startTime = Time.time;
            const float animationTime = 7f;
            const float speed = 250f;
            var distanceAdjustment = (22f - distance) / animationTime;
            yield return null;
            while (Time.time - startTime < animationTime) {
                // normalized animation progress
                var progress = (Time.time - startTime) / animationTime;
                // smoothing animation speed by applying sine curve 
                var easingCoefficient = Mathf.Sin(progress * Mathf.PI);
                // Rotate the camera around the cube counterclockwise
                Cam.transform.RotateAround(
                    Vector3.zero, // cube coordinates
                    Vector3.down, // rotation axis
                    speed * Time.deltaTime * easingCoefficient // smoothed rotation delta
                );
                
                AdjustDistance(Time.deltaTime * distanceAdjustment);
                yield return null;
            } 
        }
    }
}
