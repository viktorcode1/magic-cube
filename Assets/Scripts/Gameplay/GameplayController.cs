using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // Main class for handling gameplay
    public sealed class GameplayController : MonoBehaviour
    {
        Stack<RotationRecord> undoStack = new Stack<RotationRecord>();
        CubeModel cubeModel;
        GameplayUI gameplayUI;
        SaveGameData gameData;
        bool haveWon;
        
        public static GameplayController Instance { get; private set; }
        
        // In-game timer visibility is stored in `PlayerPrefs`
        public bool TimerVisible {
            get => timerVisible;
            set {
                timerVisible = value;
                if (gameplayUI != null) {
                    gameplayUI.GameTimer.gameObject.SetActive(value);
                }
                PlayerPrefs.SetInt(nameof(TimerVisible), value ? 1 : 0);
            }
        }
        bool timerVisible = true;

        void Awake()
        {
            Instance = this;
            if (PlayerPrefs.HasKey(nameof(TimerVisible))) {
                timerVisible = PlayerPrefs.GetInt(nameof(TimerVisible), 1) == 1;
            }
        }

        void OnDisable() { StopAllCoroutines(); }

        public void Undo()
        {
            if (undoStack.Count > 0 && !cubeModel.IsRotating) {
                var originalAction = undoStack.Pop();
                StartCoroutine(RotateCubeSlice(originalAction.PivotIndex,
                    !originalAction.PositiveRotationAngle, true));
            }
        }

        /// <summary>
        /// Initiates gameplay session by creating a new or loading saved game
        /// </summary>
        public void StartGame(CubeModel model, GameplayUI ui, SaveGameData data = null)
        {
            cubeModel = model;
            gameplayUI = ui;
            undoStack.Clear();
            SetUndoAvailability();
            if (data != null) {
                gameData = data;
                if (gameData != null) {
                    cubeModel.ApplyEnumeratedColors(gameData.Colors);
                    // Enumeration and constructor give reversed results in Stack<>
                    // So we need to reverse the saved elements list to reconstruct the stack correctly
                    var reversed = new List<RotationRecord>(gameData.UndoStack);
                    reversed.Reverse();
                    undoStack = new Stack<RotationRecord>(reversed);

                    StartCoroutine(GameplayProcess(gameData.TimeSpent));
                    return;
                }
            }
            StartCoroutine(NewGame());
        }

        public void LeaveGame()
        {
            StopAllCoroutines();
        }

        /// <summary>
        /// Starts the new game
        /// </summary>
        IEnumerator NewGame()
        {
            gameplayUI.GameTimer.gameObject.SetActive(false);
            //yield return new WaitForSeconds(2);
            cubeModel.InitializeDefaultColors();
            // I think it feels natural to allow player to move camera around while the cube is scrambled
            PlayerInput.Activate();
            PlayerInput.LockCubeManipulation(true); // We don't player to interact with cube yet
            yield return cubeModel.StartCoroutine(ScrambleCube());
            
            gameData = new SaveGameData {
                Colors = cubeModel.EnumerateColors(),
                GameType = ScenesNavigation.Instance.CurrentScreen,
                UndoStack = new List<RotationRecord>(undoStack)
            };
            gameData.Save();
            
            PlayerInput.LockCubeManipulation(false);
            yield return StartCoroutine(GameplayProcess(0));
        }

        /// <summary>
        /// Main gameplay loop
        /// </summary>
        IEnumerator GameplayProcess(float previouslySpentTime)
        {
            SetUndoAvailability();
            gameplayUI.GameTimer.gameObject.SetActive(TimerVisible);

            // Enabling game controls and waiting for it to end 
            haveWon = false;
            PlayerInput.Activate();
            var gameStartTime = Time.time;
            do {
                var timeSpent = Time.time - gameStartTime + previouslySpentTime;
                gameplayUI.GameTimer.text = TimeSpan.FromSeconds(timeSpent).ToString(@"hh\:mm\:ss");
                gameData.TimeSpent = timeSpent;
                yield return new WaitForSeconds(0.2f);
            } while (!haveWon);
            PlayerInput.Deactivate();
            
            // Let's congratulate the player
            var orbiter = Camera.main.gameObject.GetComponent<CameraOrbiter>();
            yield return StartCoroutine(orbiter.FlyAround());
            yield return StartCoroutine(ShowEndGameDialog());
            PlayerInput.Activate();
            PlayerInput.LockCubeManipulation(true); // No need to play with cube after winning 
            
            while (true) {
                yield return null;
            }
        }

        IEnumerator ShowEndGameDialog()
        {
            var task = EndGameMenu.Show(gameData.TimeSpent);
            yield return new WaitUntil(() => task.IsCompleted);
        }
        
        /// <summary>
        ///  Randomises the cube by randomly rotating it
        /// </summary>
        IEnumerator ScrambleCube()
        {
            var previous = new RotationRecord();
            for (var i = 0; i < 50; i++) {
                var pivotIndex = UnityEngine.Random.Range(0, cubeModel.PivotsCount);
                var positiveAngle = UnityEngine.Random.value > 0.5f; // random boolean
                var record = new RotationRecord { PivotIndex = pivotIndex, PositiveRotationAngle = positiveAngle };
                if (record.IsOppositeTo(previous)) {
                    i -= 1;
                    continue;
                }
                // Uncomment this line to enable undo stack during scrambling. This allows solving the cube by using undo button.
                // undoStack.Push(record);
                yield return StartCoroutine(cubeModel.RotateSlice(cubeModel.GetPivot(pivotIndex), positiveAngle, 0.2f));
                previous = record;
            }
        }

        /// <summary>
        /// Performs cube slice rotation by 90 degrees
        /// </summary>
        public IEnumerator RotateCubeSlice(int pivotIndex, bool positiveAngle, bool isUndoMove = false)
        {
            if (!isUndoMove) { // if we aren't undoing previous moves then record this as the new action 
                var record = new RotationRecord { PivotIndex = pivotIndex, PositiveRotationAngle = positiveAngle };
                undoStack.Push(record);
            }
            yield return StartCoroutine(cubeModel.RotateSlice(cubeModel.GetPivot(pivotIndex), positiveAngle));
            SetUndoAvailability();

            haveWon = cubeModel.CheckForWin();
            if (haveWon) {
                SaveGameData.DeleteSave();
            } else {
                // save the game state after each rotation 
                gameData.Colors = cubeModel.EnumerateColors();
                gameData.UndoStack = new List<RotationRecord>(undoStack);
                gameData.Save();
            }
        }
        
        /// <summary>
        /// Should undo button be enabled?
        /// </summary>
        void SetUndoAvailability() => gameplayUI.UndoButton.gameObject.SetActive(undoStack.Count != 0);
        
    }
}
