using System.Collections;
using System.Linq;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Handles player input gestures in gameplay
    /// </summary>
    public sealed class PlayerInput : MonoBehaviour
    {
        static PlayerInput instance;
        CameraOrbiter orbiter;
        CubeModel magicCube;
        bool cubeLocked;

        void Awake()
        {
            orbiter = Camera.main.gameObject.GetComponent<CameraOrbiter>();
            magicCube = GetComponent<CubeModel>();
        }

        void Start()
        {
            StartProcesses();
        }

        public void StartProcesses()
        {
            StartCoroutine(ProcessDrag());
            StartCoroutine(ProcessZoom());
        }
        
        IEnumerator ProcessDrag()
        {
            while (true) {
                // Wait for fixed update to use physics raycast
                yield return new WaitForFixedUpdate();
                
                // GetMouseButton(0) works as touch too
                if (!Input.GetMouseButton(0) || Input.touchCount > 1) continue;

                var clickPosition = Input.mousePosition;
                var ray = orbiter.Cam.ScreenPointToRay(clickPosition);
                
                // Check if touching the cube model (unless it's locked)
                if (!cubeLocked && Physics.Raycast(ray, out var hit)) { 
                    // Get the little cubie
                    var cubie = hit.collider.GetComponent<Face>().Owner;
                    // Find the two out of three pivots around which the rotation is possible by dragging
                    var pivots = magicCube.FindPivotsForCubie(cubie)
                        // Take pivots with rotation axis orthogonal to the cubie's face normal
                        .Where(p => Mathf.Approximately(Vector3.Angle(p.RotationPlane.normal, hit.normal), 90))
                        .ToArray();
                    // Get the two possible direction for dragging in 3D space
                    var rotationDirectionWorld = Vector3.Cross(pivots[0].RotationPlane.normal, hit.normal);
                    var alternativeDirectionWorld = Vector3.Cross(pivots[1].RotationPlane.normal, hit.normal);
                    // Project them on screen surface
                    var rotationDirection = DirectionOnScreen(clickPosition, hit.point, alternativeDirectionWorld); 
                    var alternativeDirection = DirectionOnScreen(clickPosition, hit.point, rotationDirectionWorld);
                    
                    float distanceAlongDirection; // how far we are dragging along possible rotation direction
                    const float threshold = 25f;  // minimum amount to drag to trigger rotation
                    var pivotIndex = 0;
                    do {
                        yield return null;

                        Vector2 mousePosition = Input.mousePosition;
                        var dragDistance = mousePosition - (Vector2)clickPosition;
                        // testing both directions and picking the first one to cross the threshold
                        distanceAlongDirection = Vector2.Dot(dragDistance, alternativeDirection);
                        if (Mathf.Abs(distanceAlongDirection) > threshold)
                            break;

                        distanceAlongDirection = Vector2.Dot(dragDistance, rotationDirection);
                        if (Mathf.Abs(distanceAlongDirection) > threshold) {
                            pivotIndex = 1;
                            break;
                        }
                    } while (Input.GetMouseButton(0));

                    // make sure that we exited previous loop due to dragging > threshold 
                    if (!Input.GetMouseButton(0)) continue; 
                    var pivot = pivots[pivotIndex];
                    // Can rotate the slice now 
                    yield return StartCoroutine(
                        GameplayController.Instance.RotateCubeSlice(
                            magicCube.GetIndex(pivot),  // around the selected pivot
                            distanceAlongDirection > 0  // in the direction of the drag
                        ));
                    
                } else { // Nothing is hit, moving camera
                    var previousPosition = orbiter.Cam.ScreenToViewportPoint(clickPosition);
                    yield return null; // Skip the frame to check if mouse is still down
                    while (Input.GetMouseButton(0)) {
                        var newPosition = orbiter.Cam.ScreenToViewportPoint(Input.mousePosition);
                        var movementDelta = previousPosition - newPosition;
            
                        orbiter.Rotate(movementDelta);
            
                        previousPosition = newPosition;
                        yield return null;
                    }
                }
            }
        }
        
        /// <summary>
        /// Projects direction vector to the screen space
        /// </summary>
        Vector2 DirectionOnScreen(Vector2 screenPoint, Vector3 worldPoint, Vector3 worldDirection) {
            Vector2 projected = orbiter.Cam.WorldToScreenPoint(worldPoint + worldDirection);

            return (projected - screenPoint).normalized;
        }
        
        /// <summary>
        /// Coroutine to handle camera distance adjustment
        /// </summary>
        IEnumerator ProcessZoom()
        {
            // Adjust sensitivity as needed
            const float wheelSensitivity = 1f;
            const float touchSensitivity = 0.04f;
            while (true) {
                // handling mouse wheel
                if (Input.mouseScrollDelta.y != 0)
                    orbiter.AdjustDistance(-Input.mouseScrollDelta.y * wheelSensitivity);
                
                // handling pinch to zoom
                if (Input.touchCount == 2) { 
                    var touchZero = Input.GetTouch(0);
                    var touchOne = Input.GetTouch(1);

                    // Find the position in the previous frame of each touch.
                    var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    var prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    var touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    var delta = (prevTouchDeltaMag - touchDeltaMag) * touchSensitivity;
                    
                    orbiter.AdjustDistance(delta);
                }
                yield return null;
            }
        }
        
        /// <summary>
        /// Enables player control
        /// </summary>
        public static void Activate()
        {
            if (instance == null)
                instance = FindObjectOfType<CubeModel>().gameObject.AddComponent<PlayerInput>();
        }

        /// <summary>
        /// Disables player control
        /// </summary>
        public static void Deactivate()
        {
            Destroy(instance);
        }

        /// <summary>
        /// Puts player control on pause
        /// </summary>
        public static void Suspend(bool suspend)
        {
            if (instance != null) {
                if (suspend) {
                    instance.StopAllCoroutines();
                } else {
                    instance.StartProcesses();
                }
            }
        }

        /// <summary>
        /// Disables or enables cube manipulation while player controlled camera is active
        /// </summary>
        /// <param name="locked"></param>
        public static void LockCubeManipulation(bool locked)
        {
            instance.cubeLocked = locked;
        }
    }
}