using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Game
{
    /// <summary>
    /// Shifts depth of field Gaussian effect to simulate camera changing focus 
    /// </summary>
    public sealed class CameraFocus : MonoBehaviour
    {
        Camera cam;
        DepthOfField depth;
        float focusDistance;
        bool shiftingFocus;
        const float focusTime = 0.5f;
        const float blurEndOffset = 20f;

        void Awake()
        {
            cam = GetComponent<Camera>();
            var volume = GetComponent<Volume>();
            volume.profile.TryGet(out depth);
        }

        void Update()
        {
            // Cube is in the center, we focus on it
            var distanceToCenter = cam.transform.position.magnitude;
            // bail out if already in focus or shifting focus
            if (Mathf.Abs(distanceToCenter - focusDistance) < 0.1 || shiftingFocus) return;
            StartCoroutine(ShiftFocus(distanceToCenter));
        }

        /// <summary>
        /// Shifting camera focus over time
        /// </summary>
        IEnumerator ShiftFocus(float targetDistance)
        {
            shiftingFocus = true;
            
            var startTime = Time.time;
            do {
                yield return null;
                depth.gaussianStart.value =
                    Mathf.Lerp(focusDistance, targetDistance, (Time.time - startTime) / focusTime);
                depth.gaussianEnd.value = depth.gaussianStart.value + blurEndOffset;
            } while (Time.time - startTime < focusTime);
            focusDistance = targetDistance;

            shiftingFocus = false;
        }
    }
}