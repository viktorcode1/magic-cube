namespace Game
{
    /// <summary>
    /// Stores single rotation action description for Undo stack
    /// </summary>
    public struct RotationRecord
    {
        public int PivotIndex;
        public bool PositiveRotationAngle;

        public bool IsOppositeTo(RotationRecord other) =>
            other.PivotIndex == PivotIndex && other.PositiveRotationAngle != PositiveRotationAngle;
    }
}