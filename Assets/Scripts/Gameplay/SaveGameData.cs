using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Serializes the game state
    /// </summary>
    public sealed class SaveGameData
    {
        static readonly XmlSerializer serializer = new XmlSerializer(typeof(SaveGameData));
        static readonly string filename = Path.Combine(Application.persistentDataPath, "save.xml");

        public Screen GameType;
        public FaceColor[] Colors;
        public List<RotationRecord> UndoStack;
        public float TimeSpent;

        public void Save()
        {
            using Stream stream = new FileStream(filename, FileMode.Create);
            using var xmlWriter = XmlWriter.Create(stream);
            serializer.Serialize(xmlWriter, this);
        }

        public static SaveGameData Load()
        {
            SaveGameData save = null;
            try {
                using Stream stream = new FileStream(filename, FileMode.Open);
                save = serializer.Deserialize(stream) as SaveGameData;
            }
            catch(Exception e)
            {
                Debug.LogError($"Error loading saved game: {e.Message}");
            }
            return save;
        }

        public static bool SaveGameDataExists() => File.Exists(filename);

        public static void DeleteSave() => File.Delete(filename);
    }
}