namespace UnityEngine
{
    public static class BoundsExtensions
    {
        /// <summary>
        /// Tests bounding box for intersecting a Plane
        /// </summary>
        public static bool IntersectsPlane(this Bounds boundingBox, Plane plane)
        {
            bool negativeFound = false, positiveFound = false;
            // Bounding box intersect a plane if its vertices aren't all on the same side
            foreach (var point in boundingBox.Vertices()) {
                var positiveSide = plane.GetSide(point);
                if (positiveSide) positiveFound = true;
                else negativeFound = true;
                // Did we find vertices on both side of the plane?
                if (positiveFound == negativeFound) return true;
            }
            return false;
        }

        /// <summary>
        /// Generates vertices array for bounding box
        /// </summary>
        public static Vector3[] Vertices(this Bounds bounds)
        {
            var vertices = new Vector3[8];
            vertices[0] = bounds.min;
            vertices[1] = bounds.max;
            vertices[2] = new Vector3(vertices[0].x, vertices[0].y, vertices[1].z);
            vertices[3] = new Vector3(vertices[0].x, vertices[1].y, vertices[0].z);
            vertices[4] = new Vector3(vertices[1].x, vertices[0].y, vertices[0].z);
            vertices[5] = new Vector3(vertices[0].x, vertices[1].y, vertices[1].z);
            vertices[6] = new Vector3(vertices[1].x, vertices[0].y, vertices[1].z);
            vertices[7] = new Vector3(vertices[1].x, vertices[1].y, vertices[0].z);

            return vertices;
        }
    }
}