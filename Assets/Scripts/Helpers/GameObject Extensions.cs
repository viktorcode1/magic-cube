using System.Collections.Generic;

namespace UnityEngine
{
    public static class GameObjectExtensions
    {
        public static void RemoveAllChildren(this GameObject parent)
        {
            foreach (Transform child in parent.transform) {
                Object.Destroy(child.gameObject);
            }
        }
        
        public static void SetParentForAll(this List<Transform> collection, Transform parent, bool preservingPosition = true)
        {
            foreach (var element in collection) {
                element.transform.SetParent(parent, preservingPosition);
            }
        }
    }
}