using System;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Routes button presses to screen navigation commands
    /// </summary>
    public sealed class NavigationButtonsHandler : MonoBehaviour
    {
        ScenesNavigation navigation;
        
        void Awake() { navigation = ScenesNavigation.Instance; }
        
        public void NavigationButtonClicked(string target)
        {
            if (Enum.TryParse(target, false, out Screen screen)) {
                navigation.NavigateTo(screen);
            }
        }

        public void ContinueButtonClicked()
        {
            navigation.ContinueGame();
        }
    }
}