using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class GameMenu : MonoBehaviour
    {
        public Toggle TimerToggle;
        GameplayController gameplay;
        readonly TaskCompletionSource<object> completion = new TaskCompletionSource<object>();

        void OnEnable()
        {
            Time.timeScale = 0;
            PlayerInput.Suspend(true);
        }

        void OnDisable()
        {
            Time.timeScale = 1;
            PlayerInput.Suspend(false);
        }
        
        void Awake() { gameplay = GameplayController.Instance; }
        
        void Start() { TimerToggle.isOn = gameplay.TimerVisible; }

        public static async void Show()
        {
            var dialogPrefab = Resources.Load<GameObject>("UI Screens/Game Menu");
            var dialog = Instantiate(dialogPrefab).GetComponent<GameMenu>();

            await dialog.completion.Task;
            Destroy(dialog.gameObject);
        }
        
        public void OnToggleValueChanged(bool isChecked) { gameplay.TimerVisible = isChecked; }
        
        public async void OnRestartGameButtonClicked()
        {
            if (await ConfirmationDialog.Show("Start New Game?")) {
                completion.SetResult(null);
                var navigation = ScenesNavigation.Instance;
                navigation.NavigateTo(navigation.CurrentScreen);
            }
        }

        public async void OnExitButtonClicked()
        {
            if (await ConfirmationDialog.Show("Exit to Main Menu?")) {
                completion.SetResult(null);
                ScenesNavigation.Instance.NavigateTo(Screen.MainMenu);
            }
        }

        public void OnBackToGameButtonClicked() { completion.SetResult(null); }
    }
}