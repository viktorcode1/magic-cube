using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class EndGameMenu : MonoBehaviour
    {
        public Text timeSpent;
        readonly TaskCompletionSource<object> completion = new TaskCompletionSource<object>();

        void OnEnable()
        {
            Time.timeScale = 0;
            PlayerInput.Suspend(true);
        }

        void OnDisable()
        {
            Time.timeScale = 1;
            PlayerInput.Suspend(false);
        }
        
        public static async Task Show(float timeSpent)
        {
            var dialogPrefab = Resources.Load<GameObject>("UI Screens/End Game Menu");
            var dialog = Instantiate(dialogPrefab).GetComponent<EndGameMenu>();
            dialog.timeSpent.text = TimeSpan.FromSeconds(timeSpent).ToString(@"hh\:mm\:ss");

            await dialog.completion.Task;
            Destroy(dialog.gameObject);
        }
        
        public void OnRestartGameButtonClicked()
        {
            completion.SetResult(null);
            var navigation = ScenesNavigation.Instance;
            navigation.NavigateTo(navigation.CurrentScreen);
        }

        public void OnExitButtonClicked()
        {
            completion.SetResult(null);
            ScenesNavigation.Instance.NavigateTo(Screen.MainMenu);
        }

        public void OnBackToGameButtonClicked() { completion.SetResult(null); }
    }
}