using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class GameplayUI : MonoBehaviour
    {
        public Text GameTimer;
        public Button UndoButton;
        GameplayController gameplay;

        void Awake() { gameplay = GameplayController.Instance; }
        
        public void UndoButtonClicked() { gameplay.Undo(); }
        
        public void GameMenuButtonClicked() { GameMenu.Show(); } 
    }
}
