using UnityEngine;

namespace Game
{
    /// <summary>
    /// Main application class.
    /// Responsible for navigation and loading other assets.
    /// </summary>
    public sealed class ScenesNavigation : MonoBehaviour
    {
        public Canvas SceneCanvas;
        public GameObject GameplayScene;
        GameplayController gameplay;
        public Screen CurrentScreen { get; private set; }
        public static ScenesNavigation Instance { get; private set; }
        
        void Awake() { Instance = this; }

        void Start()
        {
            gameplay = GameplayController.Instance;
            CurrentScreen = Screen.MainMenu;
            NavigateTo(Screen.MainMenu);
        }

        public void NavigateTo(Screen screen, SaveGameData gameData = null)
        {
            var previousScreen = CurrentScreen;

            // Loading UI
            SceneCanvas.gameObject.RemoveAllChildren();
            var uiPrefab = Resources.Load<GameObject>(screen.UIPrefabName());
            var ui = Instantiate(uiPrefab, SceneCanvas.transform, false);

            // Removing and loading Game Scene when necessary
            if (previousScreen.IsGameplay()) {
                gameplay.LeaveGame();
                GameplayScene.RemoveAllChildren();
            }
            if (screen.IsGameplay()) {
                var gameplayPrefab = Resources.Load<GameObject>(screen.GameplayPrefabName());
                var gameplayModel = Instantiate(gameplayPrefab, GameplayScene.transform, false);
                gameplay.StartGame(gameplayModel.GetComponent<CubeModel>(), ui.GetComponent<GameplayUI>(), gameData);
            } 
            
            CurrentScreen = screen;
        }
        
        public void ContinueGame()
        {
            var data = SaveGameData.Load();
            if (data != null) {
                var screen = data.GameType;
                NavigateTo(screen, data);
            }
        }
    }
}