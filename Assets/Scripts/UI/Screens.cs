namespace Game
{
    /// <summary>
    /// Screens available in the game
    /// </summary>
    public enum Screen
    {
        MainMenu, GameSelection,
        TwoLevelGame, ThreeLevelGame, FourLevelGame, FiveLevelGame, SixLevelGame
    }

    public static class ScreenExtension
    {
        public static string UIPrefabName(this Screen screen)
        {
            return screen switch {
                Screen.MainMenu => "UI Screens/Main Menu",
                Screen.GameSelection => "UI Screens/New Game Selection",
                _ => "UI Screens/Gameplay UI"
            };
        }

        public static string GameplayPrefabName(this Screen screen)
        {
            return screen switch {
                Screen.TwoLevelGame => "Cubes/2x2x2",
                Screen.ThreeLevelGame => "Cubes/3x3x3",
                Screen.FourLevelGame => "Cubes/4x4x4",
                Screen.FiveLevelGame => "Cubes/5x5x5",
                Screen.SixLevelGame => "Cubes/6x6x6",
                _ => null
            };
        }

        public static bool IsGameplay(this Screen screen) => 
            screen >= Screen.TwoLevelGame && screen <= Screen.SixLevelGame;
    }
}