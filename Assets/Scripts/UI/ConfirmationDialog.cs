using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class ConfirmationDialog : MonoBehaviour
    {
        public Text Question;
        readonly TaskCompletionSource<bool> completion = new TaskCompletionSource<bool>();
        
        public static async Task<bool> Show(string question)
        {
            var dialogPrefab = Resources.Load<GameObject>("UI Screens/Modal Confirmation Dialog");
            var dialog = Instantiate(dialogPrefab).GetComponent<ConfirmationDialog>();
            dialog.Question.text = question;
            
            var result = await dialog.completion.Task;
            Destroy(dialog.gameObject);
            return result;
        }
        
        public void OnYesButtonClicked() { completion.SetResult(true); }

        public void OnNoButtonClicked() { completion.SetResult(false); }
    }
}