using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class MainMenu : MonoBehaviour
    {
        public Button ContinueButton;

        void OnEnable() { ContinueButton.interactable = SaveGameData.SaveGameDataExists(); }
    }
}
