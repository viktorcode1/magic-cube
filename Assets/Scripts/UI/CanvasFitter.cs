using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    /// <summary>
    /// Changes UI scaling mode whenever there's switch Landscape -- Portrait
    /// </summary>
    public sealed class CanvasFitter : MonoBehaviour
    {
        public CanvasScaler GameUICanvas;
        bool isPortrait;

        void Awake()
        {
            var rect = GetComponent<RectTransform>().rect;
            isPortrait = rect.width < rect.height;
        }

        void OnRectTransformDimensionsChange()
        {
            var rect = GetComponent<RectTransform>().rect;
            if (rect.width < rect.height) {
                if (!isPortrait) {  // Switching to portrait
                    GameUICanvas.referenceResolution = new Vector2(450, 800);
                    isPortrait = true;
                }
            } else {
                if (isPortrait) {   // Switching to landscape
                    GameUICanvas.referenceResolution = new Vector2(800, 600);
                    isPortrait = false;
                }
            }
        }
    }
}
